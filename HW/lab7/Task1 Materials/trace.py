from scapy.all import *
import sys

if(len(sys.argv) > 1):
	arg_dest = sys.argv[1]
else:
	raise ValueError("No IP")

ttl = 1
destination = ''

while(destination != arg_dest):
	a = IP()
	a.dst = arg_dest
	a.ttl = ttl
	b = ICMP()
	packet = a/b
	print("sending packet...")
	resp = sr1(packet, verbose=0, timeout=5)
	if resp is None:
		print "No reply"
	elif resp[ICMP].type == 0:
		print "%d hops away: " % (a.ttl), resp['IP'].src
		print "Done", resp['IP'].src
	else :
		print "%d hops away: " % (a.ttl), resp['IP'].src
	destination = resp['IP'].src
	ttl = ttl + 1

