from scapy.all import * 

def send_reply(pkt):
	if(pkt['ICMP'].type == 8):
		iph = IP()
		iph.dst = pkt['IP'].src
		iph.src = pkt['IP'].dst
		icmp = ICMP()
		icmp.type = 0
		icmp.seq = pkt['ICMP'].seq
		icmp.id = pkt['ICMP'].id
		load = pkt['Raw'].load
		packet = iph/icmp/load
		print("Spoofing ICMP %s -> %s" % (iph.src, iph.dst))
		send(packet)

pkt = sniff(filter='icmp[icmptype] == icmp-echo', prn=send_reply)

