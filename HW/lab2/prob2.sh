#!/bin/bash
# Nicholas Barnard
# 2/5/19
# CS 445

#Setting bounds for a binary search function.
lowerBound=0
upperBound=2147483647

# -lt operator on the left is less than object on the right.
while [ $lowerBound -lt $upperBound ]
do	
	#cuts bounds inn half each time until reaching
	#secret value
	avg=$(( (($lowerBound+$upperBound)+1)/2 ))
	./prob2 $avg
	result=$?

	#Break if the upperbound value is equal to the current
	#iteration value of avg in the loop
	if [ $upperBound -eq $avg ]; then
		echo "Not found"
		break
	#Break if the answer was found.
	elif [ $result -eq 0 ]; then
		break
	#Assign upperBound to avg if avg is larger than secret
	elif [ $result -eq 1 ]; then
		upperBound=$avg
	#Assign lowerBound to avg if avg is small than secret
	elif [ $result -eq 2 ]; then
		lowerBound=$avg
	#I don't know how it broke, but it did if it got here
	else
		echo "Unknown Error"
	fi
done

