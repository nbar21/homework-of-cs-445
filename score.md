# CS 445 Score Report 

## Nicholas Barnard

|   Lab   |  Score    | Comments                                                                             |
|-------- | --------- | ------------------------------------------------------------------------------------ |
| 1       |  98/100   | Your code did not print out the address and value of the SHELL environment variable  |
| 2       |  90/100   | Since the homework was submitted late on Tuesday, you only lost 10 points.           |
| 3       |  84/150   | You lost 6 points on Task5 because you did not export the variables correctly. The correct command should be **export VARIABLE='abc'**. The custom variable should show itself. Your conclusion on Task5 is incorrect as the result. You lost 9 points on task6. You should answer whether you could make your program run your custom /bin/ls. In order to do this, you should make a script/program /bin/ls containing privileged actions and show that your program can do such actions. On task7, you lost 9 points because you did not explain why there were differences in the output. On task8, you lost 7 points because the argument should be in double quotes for both cases in order to see the difference. Overall, you lost 31 points. Since this is a late submission, only 70% of 119 is recorded. Hence, you got 84 points.|
| 4       | 65.1/100  | You lost 7 points on Task5 because you did not explain how you set up the reverse shell and why it worked. Since this is a late submission, you get 70% of 93, which is 65.1.|
| 5       |  98/150   | You lost 5 points on Task3 and 5 points on Task4 because of no explanations. Since this is a late submission, only 70% of 140 is recorded, which is 98.|
| 6       | 70/100    | You did well on this assignment. Since this is a late submission, only 70% of 100 is recorded. Hence, you get 70 for this submission.| 
| 7       | 69.3/200  | You lost 1 point on the subnet matching subtask of Task1.1B because your filter expression performs string matching instead of subnet matching. 'net 10.10.10' matches both 10.10.10.10/24 and 10.10.10.11/16, but they are on different subnets. Since this is a late submission, you only get 70% of 99 out of 200, which is 69.3. You lost 50 points on Task2.1, 25 points on Task 2.2 and 25 points on Task2.3 because the report did not contain the content of those tasks.|
| 8      | 119/125    | Your telnet and ssh interruptions seemed not successful. The ssh interruption failed partly due to the wrong port number. SSH port number is 22. For the telnet interruption, I cannot identify any cause without more diagnosis. You lost 5 points on Task2 because of the above mentioned error and the lack of scapy script as asked by the assignment. Your netwox commands had enough requirements to carry out Task2, however. You lost 1 point on Task4 because no scapy script was submitted.|


Final Exam: 93 out of 100

Problem 1: 23 out of 25
    - It is expected to have two compilations: one with "-g" and the other without "-g": -2

Problem 2: 25 out of 25

Problem 3: 25 out of 25

Problem 4: 20 out of 25
    - It will filter out all ICMP packages, instead of only ICMP echo request packages: -3
    - The message in prink still shows "Telnet", and the file name is still using "telent": -2